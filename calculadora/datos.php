<?php
    $n1 = $_GET['valorUno'];
    $n2 = $_GET['valorDos'];
    $op = $_GET['operacion'];
    $resultado = 0;
    switch ($op) {
        case "adicion":
            $resultado = $n1 + $n2;
            echo "El resultado es: <br> $n1 + $n2 = $resultado";
            break;
        case "sustraer":
            $resultado = $n1 - $n2;
            echo "$n1 - $n2 = $resultado";
            break;
        case "multiplicar":
            $resultado = $n1 * $n2;
            echo "$n1 * $n2 = $resultado";
            break;
        case "dividir":
            $resultado = $n1 / $n2;
            echo "$n1 / $n2 = $resultado";
            break;
    }
    ?>